Pod::Spec.new do |s|
  s.name = 'KnobControl'
  s.summary = 'KnobControl'
  s.ios.deployment_target = '10.0'
  s.osx.deployment_target = '10.11'
  s.tvos.deployment_target = '10.0'
  s.homepage = 'http://www.lazlo.com'
  s.version = '0.0.1'
  s.source = { :git => 'git@github.com:InvokerMaster/KnobControl.git', :tag => s.version.to_s }
  s.authors = 'Swagger Codegen'
  s.license = 'Proprietary'
  s.swift_version = '4.2'
  s.source_files = 'KnobControl'
end